import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { AlertService } from '../services/alert.service';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
export class HttpErrorInterceptor implements HttpInterceptor {

    constructor(private alert: AlertService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request)
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    let errorMessage = '';
                    if (error.error instanceof ErrorEvent) {
                        errorMessage = 'Error:' + error.error;
                    } else {
                        console.log(error);
                        errorMessage = 'Error Code:'+ error.status +' Message:'+ error.error;
                    }
                    this.alert.openErrorSnackBar(errorMessage, 'OK');
                    return throwError(errorMessage);
                })
            )
    }
}