import { ParameterResponse } from './Parameter';

export interface Api{
    api_id: string;
    api_name: string;
    api_endpoint:string;
    api_type: string;
    api_uuid:string;
    api_client_id:string;
    description: string;
    environment: string;
    paramters:ParameterResponse;

}

