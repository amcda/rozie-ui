export interface HttpResponseBody {
    data: any;
    next?: any;
    num_pages?: any;
    total_items?: any;
}