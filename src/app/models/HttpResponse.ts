export interface HttpResponse {
    code:string;
    headers: any;
    body: HttpBody;
    id: string;
    status: string;
    timestamp: string;
}

export interface HttpBody{
    data: any;
    errors: any;
    id: string;
    info: string;
    request: any;
    status: string;
    timestamp: string;
}