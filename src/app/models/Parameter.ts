export interface Parameter{
    param_name: string;
    description: string;
    sample_value: string;
    default_value: string;
    optional: string;
}

export interface ParameterResponse{
    param_id: string;
    param_name: string;
    description: string;
    sample_value: string;
    default_value: string;
    is_optional: string;
}