import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private snackBar: MatSnackBar) { }

  openErrorSnackBar(message: string, action: string){
    this.snackBar.open(message,action,{
      verticalPosition: 'top',
      horizontalPosition: 'right',
      panelClass:['alert-error']
    });
  }

  openSuccessSnackBar(message: string, action: string){
    this.snackBar.open(message,action,{
      duration:5000,
      verticalPosition: 'top',
      horizontalPosition: 'right',
      panelClass:['alert-success']
    });
  }
}
