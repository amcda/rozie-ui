import { TestBed } from '@angular/core/testing';

import { ApiAddService } from './api-add.service';

describe('ApiAddService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiAddService = TestBed.get(ApiAddService);
    expect(service).toBeTruthy();
  });
});
