import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiAddService {

  private messageSource = new BehaviorSubject<any>(undefined);
  addingApi = this.messageSource.asObservable();

  private dataSource = new BehaviorSubject<any>(undefined);
  addToskill = this.dataSource.asObservable();

  constructor() { }

  addApiToSkillMapper(api: any) {
    this.messageSource.next(api);
  }

  addApiToSkillMapperView(api: any){
    this.dataSource.next(api);
  }

}
