import { Component, OnInit, Output, EventEmitter, ViewChild, ChangeDetectorRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialog, MatDialogConfig, MatTableDataSource, MatPaginator } from "@angular/material";
import { CreateApiComponent } from 'src/app/components/create-api/create-api.component';
import { ApiAddService } from 'src/app/services/api-add.service';
import { Api } from 'src/app/models/Api';
import { SelectionModel } from '@angular/cdk/collections';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-list-api',
  templateUrl: './list-api.component.html',
  styleUrls: ['./list-api.component.scss']
})
export class ListApiComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;  
  apis: any[];
  api: any;
  apiCount: number = 5;
  message:string;
  displayedColumns: string[] = ['api_type','api_name'];
  dataSource: MatTableDataSource<any>;
  selection = new SelectionModel<Api>(false,[]);

  
  constructor(private http: HttpClient, private dialog: MatDialog,
    private apiAddService: ApiAddService, private changeDetectorRefs: ChangeDetectorRef) { }

  ngOnInit() {
    this.getAllApis();
    this.apiAddService.addingApi.subscribe(api => this.api = api);  
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onColumnSelect(row: Api){
    this.selection.toggle(row);
    this.onApiClicked(row);
  }

  getAllApis(){
    this.http.get(environment.baseUrl +'/api/get/all')
        .subscribe((response: Api[]) => {
          this.apis = response;
          this.apiCount = this.apis.length;
          this.apis.forEach(element => {
            if(element.api_type == 'post'){
               element['api_cat'] = 'POST';
               element['api_color'] ='#164e68';
            } else {
              element['api_cat'] = 'GET';
               element['api_color'] ='#6aca86';
            }
          });
          this.dataSource = new MatTableDataSource(this.apis);
          this.dataSource.paginator = this.paginator;
          console.log('All APIs ', this.apis);
        })
  }

  onShowDialog(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "90%";
    dialogConfig.position= {top: '20px'};
    this.dialog.open( CreateApiComponent,dialogConfig).afterClosed().subscribe(res => {
        this.refresh();
    });
  }

  refresh(){
    this.http.get(environment.baseUrl + '/api/get/all')
    .subscribe((response: Api[]) => {
      this.apis = response;
      this.apiCount = this.apis.length;
      this.apis.forEach(element => {
        if(element.api_type == 'post'){
           element['api_cat'] = 'POST';
           element['api_color'] ='#164e68';
        } else {
          element['api_cat'] = 'GET';
           element['api_color'] ='#6aca86';
        }
      });
      this.dataSource = new MatTableDataSource(this.apis);
      this.dataSource.paginator = this.paginator;
      this.changeDetectorRefs.detectChanges();
      console.log('All APIs ', this.apis);
    })
  }

  onApiClicked(api: Api){
    this.apiAddService.addApiToSkillMapper(api);
  }
}
