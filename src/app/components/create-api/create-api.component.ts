import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ApiType } from "../../models/ApiType";
import { Option } from "../../models/Option";
import { MatDialogRef } from "@angular/material";
import { ERROR_REQUIRED } from "../../constants/app.constants";
import { AlertService } from 'src/app/services/alert.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-create-api',
  templateUrl: './create-api.component.html',
  styleUrls: ['./create-api.component.scss']
})
export class CreateApiComponent implements OnInit {


  createApiForm: FormGroup;
  params: FormArray;
  apis: ApiType[];
  options: Option[];

  constructor(private fb: FormBuilder, private http: HttpClient,private alert: AlertService,
    public dialogRef: MatDialogRef<CreateApiComponent>) { }

  ngOnInit() {
    this.createApiForm = this.fb.group({
      api_name: ['', [Validators.required]],
      api_endpoint: ['', [Validators.required]],
      description: '',
      api_type: ['', [Validators.required]],
      client_id: "AC",
      environment: "dev",
      parameters: this.fb.array([])
    });

    this.createApiForm.valueChanges.subscribe(console.log);

    this.apis = [{ type: 'GET', value: 'get' }, { type: 'POST', value: 'post' }];
    this.options = [{ type: 'Optional', value: true }, { type: 'Required', value: false }];
  }

  createParameter(): FormGroup {
    return this.fb.group({
      param_name: ['', [Validators.required]],
      description: '',
      default_value: '',
      sample_value: '',
      optional: [ , [Validators.required]],
    });
  }

  addParameter(): void {
    this.params = this.createApiForm.get('parameters') as FormArray;
    this.params.push(this.createParameter());
  }

  deleteParameter(i) {
    this.params.removeAt(i);
  }

  get apiParameters() {
    return this.createApiForm.get("parameters") as FormArray;
  }

  onSubmit() {
    if (this.createApiForm.valid) {
      this.http.post(environment.baseUrl + '/api/register', this.createApiForm.value)
        .subscribe((response) => {
          console.log('repsonse ', response);
          this.dialogRef.close();
          this.alert.openSuccessSnackBar('API successfully created','OK');
        })
    }
  }

  onClose() {
    this.dialogRef.close();
  }

  errorRequired(){
    return ERROR_REQUIRED;
  }
}

