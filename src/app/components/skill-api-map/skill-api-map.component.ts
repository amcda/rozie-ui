import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { HttpResponseBody } from 'src/app/models/HttpResponseBody';
import { ApiAddService } from 'src/app/services/api-add.service';
import { ERROR_REQUIRED } from "../../constants/app.constants";
import { AlertService } from 'src/app/services/alert.service';
import { HttpResponse } from 'src/app/models/HttpResponse';
import { MatTableDataSource } from '@angular/material';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-skill-api-map',
  templateUrl: './skill-api-map.component.html',
  styleUrls: ['./skill-api-map.component.scss']
})
export class SkillApiMapComponent implements OnInit {

  mapApiToSkillForm: FormGroup;
  apiSet: FormArray;
  paramConcept: FormArray;
  skills: any = [];
  apis: any = [];
  concepts: any = [];
  apiExsist: boolean = false;
  apiCount: number = -1;
  skillSelected: string;

  constructor(private fb: FormBuilder, private http: HttpClient,
     private apiAddService: ApiAddService, private alert: AlertService) { }

  ngOnInit() {
    this.mapApiToSkillForm = this.fb.group({
      skill_name: ['', [Validators.required]],
      skill_uuid: ['', [Validators.required]],
      description: '',
      client_id: ['AC', [Validators.required]],
      api_set: this.fb.array([])
    });

    this.getAllSkills();
    this.getAllConcepts();
    this.mapApiToSkillForm.valueChanges.subscribe(console.log);
    this.apiAddService.addToskill.subscribe(api => this.addApi(api));

  }

  initApiSet(api: any) {
    return this.fb.group({
      api_id: api.api_id,
      api_name: api.api_name,
      api_desc: api.description,
      parameter_concept_map: this.fb.array([])
    })
  }

  initParameterConcept(param: any) {
    return this.fb.group({
      param_id: param.param_id,
      param_name: param.param_name,
      concept_name: ['', [Validators.required]]
    });
  }

  addApiSet(api: any) {
    const control = <FormArray>this.mapApiToSkillForm.controls['api_set'];
    control.push(this.initApiSet(api));
    this.apiCount++;
  }

  addParameterConcept(param: any) {
    this.mapApiToSkillForm.get('client_id').setValue('AC');
    const control = (<FormArray>this.mapApiToSkillForm.controls['api_set']).at(this.apiCount).get('parameter_concept_map') as FormArray;
    control.push(this.initParameterConcept(param));
  }

  addApi(api: any) {
    if (api) {
      this.apiExsist = true;
      this.addApiSet(api);
      api.parameters.forEach(element => {
        this.addParameterConcept(element);
      });
    }

  }

  deleteApiSet(i) {
    const control = <FormArray>this.mapApiToSkillForm.get('api_set');
    control.removeAt(i);
    this.apiCount--;
    if(control.length == 0){
        this.apiExsist = false;
    }
  }

  deleteParamConcept(i) {
    this.paramConcept.removeAt(i);
  }

  get apiSets() {
    return this.mapApiToSkillForm.get("api_set") as FormArray;
  }

  get paramConcepts() {
    return (<FormArray>this.mapApiToSkillForm.controls['api_set']).at(this.apiCount).get('parameter_concept_map') as FormArray;
  }

  removeAllInApiSet(){
    const control = this.mapApiToSkillForm.get("api_set") as FormArray;
    while (control.length !== 0) {
      control.removeAt(0)
    }
    this.apiCount = -1;
    control.controls = [];
  }

  onSubmit() {
    if (this.mapApiToSkillForm.valid) {
      this.http.post<HttpResponse>(environment.baseUrl + '/skill/create', this.mapApiToSkillForm.value)
        .subscribe((response) => {
          console.log('repsonse 1', response.code);
          if(response.code == '200'){
            this.mapApiToSkillForm.reset();
            this.removeAllInApiSet();
            this.alert.openSuccessSnackBar("Skill mapped with API(s) successfully","OK");
            window.alert(response.body);
          } else if( response.code == '500'){
            this.alert.openErrorSnackBar("Skill API mapping alrady persisted","OK");
          } else {
            this.alert.openSuccessSnackBar("Error occurred while mapping Skill with APIs","OK");
          }
        });
    }
  }

  getAllSkills() {
    this.http.get<HttpResponseBody>(environment.baseUrl +'/skill/get')
      .subscribe((response) => {
        this.skills = response.data;
        console.log('skill repsonse ', this.skills);
      });
  }

  getAllConcepts() {
    this.http.get<HttpResponseBody>(environment.baseUrl +'/concept/get')
      .subscribe(
        (response) => {
        this.concepts = response.data;
        console.log('concept repsonse ', this.concepts);
      });
  }

  onSkillSelcetChange(event) {
    var selectedSkill = this.skills.filter(item => { return item.skill_id === event.value })[0];
    if (selectedSkill) {
      this.mapApiToSkillForm.get('skill_name').setValue(selectedSkill.label);
    }
  }

  errorRequired(){
    return ERROR_REQUIRED;
  }
}
