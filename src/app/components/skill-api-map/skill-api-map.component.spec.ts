import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillApiMapComponent } from './skill-api-map.component';

describe('SkillApiMapComponent', () => {
  let component: SkillApiMapComponent;
  let fixture: ComponentFixture<SkillApiMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillApiMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillApiMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
