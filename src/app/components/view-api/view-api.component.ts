import { Component, OnInit } from '@angular/core';
import { ApiAddService } from 'src/app/services/api-add.service';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-view-api',
  templateUrl: './view-api.component.html',
  styleUrls: ['./view-api.component.scss']
})
export class ViewApiComponent implements OnInit {
  isApiSelected: boolean = false;
  api: any;
  displayedColumns: string[] = ['param_name', 'description', 'sample_value', 'default_value', 'is_optional'];
  dataSource : MatTableDataSource<any>;
  constructor(private apiAddService: ApiAddService) { }

  ngOnInit() {
    this.apiAddService.addingApi.subscribe(api => this.showApi(api));
    this.apiAddService.addToskill.subscribe(api => this.api = api);
  }

  showApi(api: any){
    if(api){
      this.api = api;
      this.dataSource = new MatTableDataSource(this.api.parameters);
      this.isApiSelected = true;
    }
  }

  onAddClicked(){
    this.apiAddService.addApiToSkillMapperView(this.api);
  }

  onCancel(){
    this.isApiSelected = false;
  }

}
