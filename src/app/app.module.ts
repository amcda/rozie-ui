import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FullLayoutComponent } from './containers/full-layout/full-layout.component';
import { ApiHomeComponent } from './views/pages/api-home/api-home.component';
import { CreateApiComponent } from './components/create-api/create-api.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSnackBarModule,
  MatTableModule,
  MatToolbarModule,
  MatDividerModule,
} from '@angular/material';


import { ReactiveFormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import { ListApiComponent } from './components/list-api/list-api.component';
import { SkillApiMapComponent } from './components/skill-api-map/skill-api-map.component';
import { ViewApiComponent } from './components/view-api/view-api.component';
import { HttpErrorInterceptor } from './helpers/http-error.interceptor';
@NgModule({
  declarations: [
    AppComponent,
    FullLayoutComponent,
    ApiHomeComponent,
    CreateApiComponent,
    ListApiComponent,
    SkillApiMapComponent,
    ViewApiComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatFormFieldModule,
    MatGridListModule,
    MatCardModule,
    MatDialogModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatDividerModule,
    MatPaginatorModule,
    MatSnackBarModule
  ],
  providers: [
  { provide: HTTP_INTERCEPTORS,
    useClass: HttpErrorInterceptor,
    multi: true  
  }
  ],
  bootstrap: [AppComponent],
  entryComponents: [CreateApiComponent]
})
export class AppModule { }
