# RozieProject

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.1.

**Note:Befor run the UI server make sure backend proxy server up and runnig.

# To Run the Development Backend Server

Please check the development server Readme.md

Node proxy server repository:

    git clone https://chathura_alahakoon@bitbucket.org/amcda/node-proxy.git

# How To install this repository

We can install the master branch using the following commands:

    git clone https://chathura_alahakoon@bitbucket.org/amcda/rozie-ui.git


Navigate the source folder by following commands:

    cd rozie-ui

Install the modules as usual using npm:

    npm install


# To run the Development UI Server

To run the frontend part of our code, use the Angular CLI:

    npm start
